#!/bin/bash -x

set -o errexit

VER=2.4.38
ctr1=$(buildah from mareksr/c7)
mountpoint=$(buildah mount $ctr1)

buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 wget http://mpm-itk.sesse.net/mpm-itk-2.4.7-04.tar.gz 
buildah run $ctr1 wget http://www.trieuvan.com/apache//httpd/httpd-$VER.tar.gz
buildah run $ctr1 wget https://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz
buildah run $ctr1 wget https://raw.githubusercontent.com/matsumotory/mod_extract_forwarded_for_2.4/master/mod_extract_forwarded.c
buildah run $ctr1 wget  "https://tn123.org/mod_xsendfile/mod_xsendfile.c#hash(sha256:8e8c21ef39bbe86464d3831fd30cc4c51633f6e2e002204509e55fc7c8df9cf9)"

buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 tar -vxf mpm-itk-2.4.7-04.tar.gz 
buildah run $ctr1 tar -vxf httpd-$VER.tar.gz 
buildah run $ctr1 tar -vxf ioncube_loaders_lin_x86-64.tar.gz

buildah config --workingdir /usr/src/httpd-$VER $ctr1
buildah run $ctr1 ./configure --prefix=/opt/httpd-$VER --with-apr=/opt/apr-1.6.5 --with-apr-util=/opt/apr-util-1.6.1 --with-mpm=prefork --enable-ssl --enable-deflate
buildah run $ctr1 make
buildah run $ctr1 make install

buildah config --workingdir /usr/src/mpm-itk-2.4.7-04 $ctr1
buildah run $ctr1 ./configure --with-apxs=/opt/httpd-$VER/bin/apxs
buildah run $ctr1 make
buildah run $ctr1 make install

buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 /opt/httpd-$VER/bin/apxs -i -a -c mod_xsendfile.c
buildah run $ctr1 /opt/httpd-$VER/bin/apxs -i -a -c mod_extract_forwarded.c

#and here comes the mouse
ctr2=$(buildah from centos)
buildah config --label maintainer="Marek Sroczynski <mareksr@hm.pl>" $ctr2
buildah copy $ctr2 $mountpoint/opt/httpd-$VER /opt/httpd-$VER
buildah copy $ctr2 $mountpoint/opt/apr-util-1.6.1 /opt/apr-util-1.6.1
buildah copy $ctr2 $mountpoint/opt/apr-1.6.5 /opt/apr-1.6.5
buildah copy $ctr2 $mountpoint/usr/src/ioncube /usr/src/ioncube

buildah run $ctr2 ln -s /opt/httpd-$VER /usr/local/apache2

buildah run $ctr2 sed -i 's/^LoadModule extract_forwarded_module/#LoadModule extract_forwarded_module/' /opt/httpd-2.4.38/conf/httpd.conf


buildah run $ctr2 /sbin/groupadd -g 80  www
buildah run $ctr2 /sbin/useradd -g 80 -u 80 -b /var www
buildah run $ctr2 mkdir /logs

buildah config --cmd "/usr/local/apache2/bin//httpd -D FOREGROUND" $ctr2
buildah config --port 80 "$ctr2"

#do it !
buildah commit --format docker "$ctr2" "mareksr/httpd-$VER"


buildah unmount $ctr2
buildah rm $ctr2

buildah unmount $ctr1
buildah rm $ctr1
#push to docker
buildah push localhost/mareksr/httpd-$VER docker-daemon:mareksr/httpd:$VER