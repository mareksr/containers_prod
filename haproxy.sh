#!/bin/bash -x

set -o errexit

ctr1=$(buildah from mareksr/c7)
mountpoint=$(buildah mount $ctr1)

SSLVERSION=openssl-1.0.2r
PROXY_VER=1.9.6

buildah config --workingdir /usr/src $ctr1
buildah run $ctr1  wget https://www.openssl.org/source/$SSLVERSION.tar.gz -O $SSLVERSION.tar.gz
buildah run $ctr1  tar -vxf $SSLVERSION.tar.gz
buildah config --workingdir /usr/src/$SSLVERSION $ctr1
buildah run $ctr1  ./config  --openssldir=/usr/local/$SSLVERSION
buildah run $ctr1  make
buildah run $ctr1  make install


buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 wget http://www.haproxy.org/download/1.9/src/haproxy-$PROXY_VER.tar.gz
buildah run $ctr1 tar -vxf haproxy-$PROXY_VER.tar.gz
buildah config --workingdir /usr/src/haproxy-$PROXY_VER $ctr1
buildah run $ctr1 make USE_OPENSSL=1 TARGET=linux2628 SSL_INC=/usr/local/$SSLVERSION/include SSL_LIB=/usr/local/$SSLVERSION/lib ADDLIB=-ldl

#and here comes the mouse
ctr2=$(buildah from centos)
buildah config --label maintainer="Marek Sroczynski <mareksr@hm.pl>" $ctr2
buildah copy $ctr2 $mountpoint/usr/local/$SSLVERSION /usr/local/$SSLVERSION
buildah copy $ctr2 $mountpoint/usr/src/haproxy-$PROXY_VER/haproxy /sbin/haproxy
buildah run $ctr2 yum -y install openssl

buildah config --port 80 "$ctr2"
buildah config --port 81 "$ctr2"
buildah config --port 443 "$ctr2"
buildah config --port 444 "$ctr2"

buildah config --cmd "/sbin/haproxy -f /etc/haproxy/haproxy.cfg" $ctr2

#do it !
buildah commit --format docker "$ctr2" "mareksr/haproxy:$PROXY_VER"

buildah unmount $ctr1
buildah rm $ctr1
buildah rm $ctr2

buildah push localhost/mareksr/haproxy docker-daemon:mareksr/haproxy:$PROXY_VER