#!/bin/bash -x

set -o errexit

HTTPD_VER=2.4.38

ctr1=$(buildah from mareksr/c7)
mountpoint=$(buildah mount $ctr1)


ctr2=$(buildah from mareksr/httpd:$HTTPD_VER)
mountpoint2=$(buildah mount $ctr2)



#copy apache from httpd container
buildah copy $ctr1 $mountpoint2/opt/httpd-$HTTPD_VER /opt/httpd-$HTTPD_VER
buildah copy $ctr1 $mountpoint2/usr/src/ioncube /usr/src/ioncube

#yum
buildah run $ctr1 yum -y install mariadb-devel mariadb-libs mariadb-server libpng-devel libjpeg-devel libtomcrypt-devel 



buildah run $ctr1 sed -i 's/^LoadModule extract_forwarded_module/#LoadModule extract_forwarded_module/' /opt/httpd-$HTTPD_VER/conf/httpd.conf


buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 wget http://at2.php.net/get/php-5.3.29.tar.gz/from/this/mirror -O php-5.3.29.tar.gz


buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 wget "https://raw.githubusercontent.com/php-build/php-build/master/share/php-build/patches/php-5.3.29-64bit-intl.patch" -O php-5.3.29-64bit-intl.patch



buildah config --workingdir /usr/src $ctr1
buildah run $ctr1 tar -vxf  php-5.3.29.tar.gz


buildah config --workingdir /usr/src/php-5.3.29 $ctr1

#API 2.2 -> 2.4 change
#buildah run $ctr1 sed -i 's/unixd_config\./ap_unixd_config\./g' sapi/apache2handler/php_functions.c 
#buildah run $ctr1 sed -i 's/ap_get_server_version/ap_get_server_banner/g' sapi/apache2handler/php_functions.c 

buildah run $ctr1 bash -c 'cp -Rp /usr/local/libpng-1.6.36/lib/* /usr/lib64'
buildah run $ctr1 bash -c 'patch -p1 < /usr/src/php-5.3.29-64bit-intl.patch'



buildah run $ctr1 ./configure '--prefix=/opt/php-5.3.29' --with-apxs2=/opt/httpd-$HTTPD_VER/bin/apxs  '--enable-exif' '--enable-gd-native-ttf'  \
 '--with-xsl' '--with-mysql=shared'   '--enable-mbstring' '--with-gd' '--with-zlib'  '--with-pdo-mysql' '--with-mysqli=shared' \
 '--with-freetype-dir' '--with-curl'  '--enable-soap'  '--enable-zip=/usr/local/lib64/' '--enable-ftp' '--with-gettext'  '--enable-bcmath' \
 '--with-jpeg-dir=/lib64' --with-png-dir=/usr/local/libpng-1.6.36 --with-libdir=lib64 --with-openssl --enable-intl


buildah run $ctr1 make
buildah run $ctr1 make install

#and here comes the mouse
ctr3=$(buildah from centos)
buildah config --label maintainer="Marek Sroczynski <mareksr@hm.pl>" $ctr3

buildah copy $ctr3 $mountpoint/opt/php-5.3.29 /opt/php-5.3.29
buildah copy $ctr3 $mountpoint/opt/httpd-$HTTPD_VER /opt/httpd-$HTTPD_VER
buildah copy $ctr3 $mountpoint/opt/apr-util-1.6.1 /opt/apr-util-1.6.1
buildah copy $ctr3 $mountpoint/opt/apr-1.6.5 /opt/apr-1.6.5
buildah copy $ctr3 $mountpoint/opt/libmcrypt-2.5.8 /opt/libmcrypt-2.5.8
buildah copy $ctr3 $mountpoint/usr/local/libpng-1.6.36 /usr/local/libpng-1.6.36
buildah copy $ctr3 $mountpoint/lib64/libfreetype.so* /lib64/
buildah copy $ctr3 $mountpoint/lib64/libpng* /lib64/
buildah copy $ctr3 $mountpoint/lib64/libexslt.so* /lib64/
buildah copy $ctr3 $mountpoint/lib64/libjpeg.so* /lib64/
buildah copy $ctr3 $mountpoint/usr/lib64/mysql /usr/lib64/mysql
buildah copy $ctr3 $mountpoint/lib64/libxslt.so* /lib64
buildah copy $ctr3 $mountpoint/lib64/libicu* /lib64
 


buildah run $ctr3 mkdir -p /usr/local/Zend2/lib/ioncube
buildah copy $ctr3 $mountpoint/usr/src/ioncube /usr/local/Zend2/lib/ioncube


buildah run $ctr3 ln -s /opt/php-5.3.29 /usr/local/php4
buildah run $ctr3 ln -s  /opt/httpd-$HTTPD_VER /usr/local/apache2


buildah run $ctr3 /sbin/groupadd -g 80  www
buildah run $ctr3 /sbin/useradd -g 80 -u 80 -b /var www
buildah run $ctr3 mkdir /logs
#
buildah config --cmd "/usr/local/apache2/bin//httpd -D FOREGROUND" $ctr3
buildah config --port 80 "$ctr3"

#do it !
buildah commit --format docker "$ctr3" "mareksr/php53"


#clean
buildah unmount $ctr3
buildah rm $ctr3

buildah unmount $ctr2
buildah rm $ctr2

buildah unmount $ctr1
buildah rm $ctr1

#push to docker
buildah push localhost/mareksr/php53 docker-daemon:mareksr/php53:$HTTPD_VER
